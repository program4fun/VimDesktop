﻿; IMPORTANT INFO ABOUT GETTING STARTED: Lines that start with a
; semicolon, such as this one, are comments.  They are not executed.

; This script has a special filename and path because it is automatically
; launched when you run the program directly.  Also, any text file whose
; name ends in .ahk is associated with the program, which means that it
; can be launched simply by double-clicking it.  You can have as many .ahk
; files as you want, located in any folder.  You can also run more than
; one .ahk file simultaneously and each will get its own tray icon.

; SAMPLE HOTKEYS: Below are two sample hotkeys.  The first is Win+Z and it
; launches a web site in the default browser.  The second is Control+Alt+N
; and it launches a new Notepad window (or activates an existing one).  To
; try out these hotkeys, run AutoHotkey again, which will load this file.
; #z::Run www.autohotkey.com
; + shift

; ^!n::
; IfWinExist Untitled - Notepad
; 	WinActivate
; else
; 	Run Notepad
; return
`:: Send {ESC}
^!r::Reload
; 双击左Shift 把鼠标移动到 0,0 的位置
~LShift::
	if (A_PriorHotkey <> "~LShift" or A_TimeSincePriorHotkey > 200)
	{
		KeyWait, LShift
		return
	}
	MouseMove 0,0
return

~d::
	SetTitleMatchMode 2
	IfWinActive,Adobe Reader
	{
		Send {Down 30}
	}
	IfWinActive,福昕阅读器
	{
		Send {Down 10}
	}
return

~^F::
	SetTitleMatchMode, 2
	IfWinActive, 有道词典
	{
		; MsgBox "is YouDao"
		CoordMode, Mouse, Relative
		Click 500, 10
		Click 37, 142
		CoordMode, ToolTip, Screen
		
	}
return

 ~^G::
	IfWinActive,有道单词本
	{
		; 点击有道单词本 开始复习 按钮
		MouseMove, 370, 370
		Click
		return
	}
return

~h::
	SetTitleMatchMode 2
	IfWinActive,美图看看
	{
		Send {Left}
	}
return

~^I::
;	WinGetClass, class,A
;	MsgBox, The active window's class is "%class%".
	SetTitleMatchMode, 2
	IfWinActive, ahk_class YdMiniModeWndClassName
	{
		WinGetActiveStats, Title, Width, Height, X, Y
		; X2 := X + Width
		; Y2 := Y + Height
		; MsgBox, The active window "%Title%" is %Width% wide`, %Height% tall`, and positioned at %X%`,%Y%. X2 is %X2%. Y2 is %Y2%
		; CoordMode Pixel
		; ImageSearch, FoundX, FoundY, X, Y, X2, Y2, d:\k\pf\autohotKey\Images\add.jpg
		ImageSearch, FoundX, FoundY, 0, 0, Width, Height, d:\k\pf\autohotKey\Images\Add.png
		if ErrorLevel = 2
		    MsgBox Could not conduct the search.
		else if ErrorLevel = 1
		    MsgBox Icon could not be found on the screen. Width is %Width%. Height is %Height%.
		else
		{
			CoordMode, Mouse, Relative
			MouseMove, FoundX, FoundY
			Click FoundX, FoundY
			send {alt down}{3}{alt up}
			send {esc}
			WinGetPos, X, Y, Width, Height, Program Manager
			CoordMode, Mouse, Screen
			MouseMove, Width / 2, Height / 2
			return
			; MsgBox,Width is %Width%. Height is %Height%. FoundX is "%FoundX%". FoundY is "%FoundY%".
		}
		; WinGetPos, X, Y, Width, Height, Calculator
		; MouseMove, 10, 80
		; CoordMode, Mouse, Relative
		; MsgBox, The active window "%Title%" is %Width% wide`, %Height% tall`, and positioned at %X%`,%Y%.
		; MsgBox, "This window's x is %X%, y is %Y%"
		; MsgBox, "This window's is YouDao"
	}
	IfWinActive,美图看看
	{
		; WinGetActiveStats, Title, Width, Height, X, Y
		WinGetPos, X, Y, Width, Height, Program Manager
		CoordMode, Mouse, Screen
		MouseMove, Width / 2, Height / 2
		return
		; MsgBox, %Width%. %Height%
		; ImageSearch, FoundX, FoundY, 0, 0, 600, 200, d:\k\pf\autohotKey\Images\Add.png
		; if ErrorLevel = 2
		;     MsgBox Could not conduct the search.
		; else if ErrorLevel = 1
		;     MsgBox Icon could not be found on the screen. Width is %Width%. Height is %Height%.
		; else
		; 	MsgBox, FoundX is "%FoundX%". FoundY is "%FoundY%".
	}
	IfWinActive, 好友照片
	{
		MouseMove, 50, 580
		Click
		return
	}
	IfWinActive, SwitchyOmega Options - Google Chrome
	{
		MouseMove, 80, 680
		Click
		return
	}
return

~^J::
	IfWinActive, 有道单词本
	{
		MouseMove, 280, 400
		Click
		return
	}
return

~j::
	SetTitleMatchMode 2
	IfWinActive,Adobe Reader
	{
		Send {Down}
		return
	}
	IfWinActive, 福昕阅读器
	{
		Send {Down}
		return
	}
	IfWinActive, 有道单词本
	{
		MouseMove, 280, 400
		Click
		return
	}
	; if (A_PriorHotkey <> "~j" or A_TimeSincePriorHotkey > 160)
	; {
	; 	KeyWait, j
	; 	return
	; }
return

~^K::
	IfWinActive, 有道单词本
	{
		MouseMove, 480, 400
		Click
		return
	}
return

~k::
	SetTitleMatchMode 2
	IfWinActive,Adobe Reader
	{
		Send {Up}
		return
	}
	IfWinActive, 福昕阅读器
	{
		Send {Up}
		return
	}
return

~l::
	SetTitleMatchMode 2
	IfWinActive,美图看看
	{
		Send {Right}
	}
return

~^N::
	IfWinActive, 好友照片
	{
		Send {Down}
		return
	}
return

~^P::
	{
		IfWinActive, 好友照片
		{
			Send {Up}
			return
		}
	}
return

~^R::
	IfWinActive,有道单词本
	{
		; 点击有道单词本 复习 按钮
		MouseMove, 53, 43
		Click
		return
	}
return

#s:: 
	{
		IfWinExist, Volume Mixer - Speakers (Realtek High Definition Audio)
		{
			Run sndvol	
			return
		}
		WinActivate, Volume Mixer - Speakers (Realtek High Definition Audio)
	}
return

~u::
	SetTitleMatchMode 2
	IfWinActive,Adobe Reader
	{
		Send {Up 30}
	}
	IfWinActive,福昕阅读器
	{
		Send {Up 10}
	}
return

~V::
	IfWinActive, 有道单词本
	{
		WinGetActiveStats, Title, Width, Height, X, Y
		ImageSearch, FoundX, FoundY, 0, 0, Width, Height, d:\k\pf\autohotKey\Images\Voice.png
		if ErrorLevel = 2
		    MsgBox Could not conduct the search.
		else if ErrorLevel = 1
		    MsgBox Icon could not be found on the screen. Width is %Width%. Height is %Height%.
		else
		{
			CoordMode, Mouse, Relative
			MouseMove, FoundX, FoundY
			Click
			return
		}
	}
return

~^W::
	IfWinActive, 有道单词本
	{
		WinClose
		return
	}
	IfWinActive, 搜狗壁纸
	{
		WinClose
		return
	}
	IfWinActive, Volume Mixer - Speakers (Realtek High Definition Audio)
	{
		WinClose
		return
	}
	ifwinactive, 好友照片
	{
		WinClose
		return
	}
return


::iom::imbedinlove@gmail.com
; ^n:: Send {Down}
; `n:: Send {ESC}
; ^p:: Send {Up}
; ^f:: Send {Left}
; ^b:: Send {Right}
; CapsLock::SendInput {Esc}
; Esc::CapsLock
; #n:: WinMinimize A
; #x:: WinMaximize A

^!F12:: 
  IfWinExist 192.168.1.24 - Remote Desktop Connection
	WinActivate 
  else
	Run d:\k\battool\24.bat
	WinWait 192.168.1.24 - Remote Desktop Connection
	WinActivate
return

; Note: From now on whenever you run AutoHotkey directly, this script
; will be loaded.  So feel free to customize it to suit your needs.

; Please read the QUICK-START TUTORIAL near the top of the help file.
; It explains how to perform common automation tasks such as sending
; keystrokes and mouse clicks.  It also explains more about hotkeys.


